//
//  AE_Blackmagic.h
//  juce_blackmagic
//
//  Created by Louis Mustill on 18/08/2014.
//
//

#ifndef __juce_blackmagic__AE_Blackmagic__
#define __juce_blackmagic__AE_Blackmagic__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "DeckLinkAPI.h"

//These define the capture mode (i.e. TV format) and format (i.e. colour representation, pixel packing). See the Blackmagic SDK documentation for your options. It's important to note that not every TV format is compatible with every colour pack - your colour format is basically locked to the TV format (the Decklink won't do any colour conversion)...

#define AE_DECKLINK_CAPTURE_MODE bmdModeHD1080i5994
#define AE_DECKLINK_CAPTURE_FORMAT bmdFormat8BitYUV

//These define a region of interest which we'll process from YUV to RGB and shove into a juce::Image
#define AE_DECKLINK_ROI_TOP_LEFT_X 1100
#define AE_DECKLINK_ROI_TOP_LEFT_Y 500
#define AE_DECKLINK_ROI_WIDTH 25
#define AE_DECKLINK_ROI_HEIGHT 250

class AE_Blackmagic : public IDeckLinkInputCallback {
public:
    AE_Blackmagic();
    ~AE_Blackmagic();
    
    virtual void videoFrameReceived(juce::Image videoFrame) = 0; //This gets called every time a new frame arrives. Override it to get pictures.
    
    bool startDecklinkStream(); //returns true if stream was started, false if it failed.
    bool stopDecklinkStream(); //returns true if stream was stopped, false if it wasn't.
    
    uint16_t deckLinkROITopLeftX, deckLinkROITopLeftY;
    uint16_t deckLinkROIWidth, decklinkROIHeight;
    
private:
    std::string CFStringToString(CFStringRef str); //utility method in which CFStrings are converted to std::strings.
    
    IDeckLink *mDeckLink; //Our Decklink device.
    IDeckLinkInput *mDeckLinkInput; //The input on our Decklink device.
    
    virtual HRESULT VideoInputFrameArrived (IDeckLinkVideoInputFrame* arrivedFrame, IDeckLinkAudioInputPacket*);
    virtual HRESULT	VideoInputFormatChanged (BMDVideoInputFormatChangedEvents notificationEvents, IDeckLinkDisplayMode* newDisplayMode, BMDDetectedVideoInputFormatFlags detectedSignalFlags);
    
    HRESULT			QueryInterface (REFIID iid, LPVOID *ppv)	{return E_NOINTERFACE;}
	ULONG			AddRef ()									{return 1;}
	ULONG			Release ()									{return 1;}
};


#endif /* defined(__juce_blackmagic__AE_Blackmagic__) */
