//
//  AE_Blackmagic.cpp
//  juce_blackmagic
//
//  Created by Louis Mustill on 18/08/2014.
//
//

#include "AE_Blackmagic.h"

std::string AE_Blackmagic::CFStringToString(CFStringRef str) {
    char result[4096];
    if (CFStringGetCString(str, result, 4095, kCFStringEncodingUTF8)) {
        return std::string(result);
    }
    
    return std::string();

}

AE_Blackmagic::AE_Blackmagic() {
    //Get a DeckLink iterator (which allows us to see Blackmagic devices attached to our system).
    IDeckLinkIterator *deckLinkIterator = CreateDeckLinkIteratorInstance();
    if (deckLinkIterator == nullptr) {
        std::cout << "Couldn't create Decklink Iterator. This usually happens when Decklink drivers have not been installed" << std::endl;
        return;
    }
    
    //Assume we only have a single DeckLink device connected by getting the first one.
    deckLinkIterator->Next(&mDeckLink);
    if (mDeckLink == nullptr) {
        std::cout << "Couldn't get default Decklink device. Is it plugged in?" << std::endl;
        return;
    }
    
    //Get a reference to the input.
    if (mDeckLink->QueryInterface(IID_IDeckLinkInput, (void**)&mDeckLinkInput) == S_OK) {
        std::cout << "Got input interface." << std::endl;
    } else {
        std::cout << "Couldn't get input interface." << std::endl;
        return;
    }
    
    //Print available input display modes.
    CFStringRef str;
    mDeckLink->GetDisplayName(&str);
    std::cout << "Using Decklink device: " << CFStringToString(str) << std::endl;
    IDeckLinkDisplayModeIterator *deckLinkDisplayModeIterator;
    mDeckLinkInput->GetDisplayModeIterator(&deckLinkDisplayModeIterator);
    IDeckLinkDisplayMode *dm;
    std::cout << "Supported display modes:\n";
    while (deckLinkDisplayModeIterator->Next(&dm) == S_OK) {
        dm->GetName(&str);
        std::cout << CFStringToString(str) << "\n";
    }
    
    std::cout << std::endl;
    
    //Register callback for picture RX
    if (mDeckLinkInput->SetCallback((IDeckLinkInputCallback*)this) == S_OK) {
        std::cout << "Registered callback for input video." << std::endl;
    } else {
        std::cout << "Couldn't register callback for input video." << std::endl;
        return;
    }
    
    //Enable video input. This is where we set our mode and format options.
    if (mDeckLinkInput->EnableVideoInput(AE_DECKLINK_CAPTURE_MODE, AE_DECKLINK_CAPTURE_FORMAT, 0) == S_OK) {
        std::cout << "Video input enabled." << std::endl;
    } else {
        std::cout << "Couldn't enable video input." << std::endl;
        return;
    }
    
    deckLinkROITopLeftX = AE_DECKLINK_ROI_TOP_LEFT_X;
    deckLinkROITopLeftY = AE_DECKLINK_ROI_TOP_LEFT_Y;
    deckLinkROIWidth = AE_DECKLINK_ROI_WIDTH;
    decklinkROIHeight = AE_DECKLINK_ROI_HEIGHT;
    
    deckLinkIterator->Release();
    
}

bool AE_Blackmagic::startDecklinkStream() {
    if (mDeckLinkInput->StartStreams() == S_OK) {
        std::cout << "Started Decklink stream." << std::endl;
        return true;
    }
    
    std::cout << "Failed to start Decklink stream." << std::endl;
    return false;
}

bool AE_Blackmagic::stopDecklinkStream() {
    if (mDeckLinkInput->StopStreams() == S_OK) {
        std::cout << "Stopped Decklink stream." << std::endl;
        return true;
    }
    
    std::cout << "Failed to stop Decklink stream." << std::endl;
    return false;
}

int max(int a, int b) {
    if (a > b) {
        return a;
    } else {
        return b;
    }
}

int min(int a, int b) {
    if (a < b) {
        return a;
    } else {
        return b;
    }
}

HRESULT AE_Blackmagic::VideoInputFrameArrived (IDeckLinkVideoInputFrame* arrivedFrame, IDeckLinkAudioInputPacket*) {
    juce::Image juceImage(juce::Image::PixelFormat::RGB, deckLinkROIWidth, decklinkROIHeight, true);
    juce::Image::BitmapData juceBitmap(juceImage, juce::Image::BitmapData::writeOnly);
    uint32_t *rgba = (uint32_t*)juceBitmap.data;
    
    uint8_t *yuv = NULL;
    arrivedFrame->GetBytes((void**)&yuv);
    arrivedFrame->AddRef();
    uint8_t Cr, Cb, y0, y1;
    uint8_t r1, g1, b1, r2, g2, b2;
    
    jassert(deckLinkROITopLeftX + deckLinkROIWidth <= arrivedFrame->GetWidth());
    jassert((deckLinkROITopLeftY + decklinkROIHeight <= arrivedFrame->GetHeight()));
    
    //we consume four bytes (two pixels) of YUV image data at a time, generating eight bytes (two rgba pixels) of RGB data each time
    int stride = arrivedFrame->GetRowBytes() / arrivedFrame->GetWidth();
    for (uint16_t row = deckLinkROITopLeftY; row < deckLinkROITopLeftY + decklinkROIHeight; row++) {
        for (uint16_t col = deckLinkROITopLeftX; col < deckLinkROITopLeftX + deckLinkROIWidth * stride; col+=4) {
            Cb = yuv[(row * arrivedFrame->GetRowBytes()) + col + 0];
            y0 = yuv[(row * arrivedFrame->GetRowBytes()) + col + 1];
            Cr = yuv[(row * arrivedFrame->GetRowBytes()) + col + 2];
            y1 = yuv[(row * arrivedFrame->GetRowBytes()) + col + 3];
            
            //magic YUV to RGB constants.
            r1 = max(0, min(255, (int)(y0 + 1.402 * (Cr - 0x80))));
            g1 = max(0, min(255, (int)(y0 + 0.34414 * (Cb - 0x80) - 0.71414 * (Cr - 0x80))));
            b1 = max(0, min(255, (int)(y0 + 1.77200 * (Cb - 0x80))));
            r2 = max(0, min(255, (int)(y1 + 1.402 * (Cr - 0x80))));
            g2 = max(0, min(255, (int)(y1 + 0.34414 * (Cb - 0x80) - 0.71414 * (Cr - 0x80))));
            b2 = max(0, min(255, (int)(y1 + 1.77200 * (Cb - 0x80))));
            
            rgba[((row - deckLinkROITopLeftY) * juceImage.getWidth()) + ((col - deckLinkROITopLeftX) / 2) + 0] =
                    (0xff << 24) | (r1 << 16) | (g1 << 8) | b1; //MSB is alpha
            rgba[((row - deckLinkROITopLeftY) * juceImage.getWidth()) + ((col - deckLinkROITopLeftX) / 2) + 1] =
                    (0xff << 24) | (r2 << 16) | (g2 << 8) | b2;
            
        }
    }
    arrivedFrame->Release();
    videoFrameReceived(juceImage);
    
    return S_OK;
}

HRESULT	AE_Blackmagic::VideoInputFormatChanged (BMDVideoInputFormatChangedEvents notificationEvents, IDeckLinkDisplayMode* newDisplayMode, BMDDetectedVideoInputFormatFlags detectedSignalFlags) {
    return S_OK;
}


AE_Blackmagic::~AE_Blackmagic() {
    if (mDeckLinkInput) {
        stopDecklinkStream();
        mDeckLinkInput->Release();
        mDeckLink->Release();
    }
}