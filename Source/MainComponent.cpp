/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================
MainContentComponent::MainContentComponent()
{
    setSize (1920, 1080);
    startDecklinkStream();
}

MainContentComponent::~MainContentComponent()
{
    stopDecklinkStream();
}

void MainContentComponent::videoFrameReceived(juce::Image videoFrame) {
    currentFrame = videoFrame;
    
    MessageManagerLock mml;
    
    if (! mml.lockWasGained()) return;
    
    repaint();
}

void MainContentComponent::paint (Graphics& g)
{
    g.fillAll (Colour (0x00000000));
    
    g.setOpacity(1.0);
    g.drawImage(currentFrame, 0, 0, currentFrame.getWidth(), currentFrame.getHeight(), 0, 0, currentFrame.getWidth(), currentFrame.getHeight());
    
}

void MainContentComponent::resized()
{
    // This is called when the MainContentComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
}
